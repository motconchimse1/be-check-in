const catchAsync = require("../utils/catchAsync");
const Models = require("../models/sequelize");

class OptionController {
  constructor(sequelize) {
    // Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  getAllOption = catchAsync(async (req, res, next) => {
    const doc = await this.client.query(
      `select * from t_option where optiongroupid = ${req.params.groupId}`
    );
    res.status(200).json({
      status: "success",
      data: doc[1].rows,
    });
  });
}

module.exports = OptionController;
