const catchAsync = require("../utils/catchAsync");
const Models = require("../models/sequelize");
const { Op } = require("sequelize");

class HandleFactory {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  checkStudentAttend = catchAsync(async (req, res, next) => {
    try {
      const { date, studentid } = req.body;
      const query = {
        include: [
          {
            model: this.models.Student,
            attributes: ["id", "code", "name", "orgunit"],
            where: {
              id: studentid,
            },
          },
        ],
        where: {
          date: {
            [Op.gte]: date[0],
            [Op.lte]: date[1],
          },
        },
      };
      const findAll = await this.models.StudentAttendance.count(query);
      if (findAll == 0) {
        res.status(200).json({
          status: "success",
          bool: true,
        });
      } else {
        res.status(200).json({
          status: "success",
          bool: false,
        });
      }
    } catch (error) {
      next();
    }
  });
}

module.exports = HandleFactory;
