const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const Models = require("../models/sequelize");
const studentAttend = require("../templates/studentAttend");
const excelJS = require("exceljs");
const { Op } = require("sequelize");
const moment = require("moment");

const orgunit = ["BMES", "CEC", "DIGIHUBS", "ĐIỆN TOÁN", "TRIS"];
class StudentAttendController {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  getStudentAttends = catchAsync(async (req, res, next) => {
    const { page, per_page, name, status, fromDay, toDay } = req.query;
    const skip = (page - 1) * per_page;
    const whereRoot = {};
    if (status) Object.assign(whereRoot, { status });
    if (fromDay && toDay)
      Object.assign(whereRoot, {
        date: {
          [Op.gte]: fromDay,
          [Op.lte]: toDay,
        },
      });
    const whereChild = {};
    if (name) Object.assign(whereChild, { name });

    const countQuery = {
      include: [
        {
          model: this.models.Student,
          attributes: ["id", "code", "name", "orgunit"],
          where: whereChild,
        },
      ],
      where: whereRoot,
    };
    const findAll = await this.models.StudentAttendance.count(countQuery);
    const query = {
      attributes: { exclude: ["studentid", "StudentId"] },
      include: [
        {
          model: this.models.Student,
          attributes: ["id", "code", "name", "orgunit"],
          where: whereChild,
        },
      ],
      where: whereRoot,
      order: [
        ["Student", "orgunit", "ASC"],
        ["Student", "name", "ASC"],
        ["date", "ASC"],
      ],
      offset: skip,
      limit: per_page,
    };

    const doc = await this.models.StudentAttendance.findAll(query);
    const pagi = {
      current: req.query.page * 1 || 1,
      pageSize: req.query.per_page * 1 || 20,
      total: findAll,
    };
    res.status(200).json({
      status: "success",
      data: doc,
      ...pagi,
    });
  });

  getStudentAttends2 = catchAsync(async (req, res, next) => {
    const { fromDay, toDay } = req.params;
    let lastDay;
    if (fromDay !== "null" && toDay !== "null") {
      const randomUser = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${fromDay}', '${toDay}') order by RANDOM()
      limit 1`
      );
      lastDay = await this.models.StudentAttendance.findAll({
        attributes: { exclude: ["studentid", "StudentId"] },
        where: {
          date: {
            [Op.gte]: fromDay,
            [Op.lte]: toDay,
          },
        },
        include: [
          {
            model: this.models.Student,
            where: {
              name: randomUser[0][0].studentname,
            },
            attributes: ["id", "code", "name", "orgunit"],
          },
        ],
        order: [["date", "ASC"]],
      });
      if (lastDay.length) {
        const date1 = new Date(fromDay);
        const date2 = new Date(lastDay[lastDay.length - 1].date);
        const difference = date2.getTime() - date1.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24));
        const bigData = await this.models.StudentAttendance.findAll({
          attributes: { exclude: ["studentid", "StudentId"] },
          where: {
            date: {
              [Op.gte]: fromDay,
              [Op.lte]: toDay,
            },
          },
          include: [
            {
              model: this.models.Student,

              attributes: ["id", "code", "name", "orgunit"],
            },
          ],
          order: [
            ["Student", "orgunit", "ASC"],
            ["Student", "name", "ASC"],
            ["date", "ASC"],
          ],
        });

        let arr = [];
        let count = 1;
        let obj = {};
        for (let i = 0; i < bigData.length; i++) {
          let nextI = i + 1;
          if (i == bigData.length - 1) {
            nextI = i;
          }
          Object.assign(obj, {
            [new Date(bigData[i].date).getDate()]: {
              attendance_id: bigData[i].id,
              val: bigData[i].status,
              eventdate: bigData[i].date,
              eventtime: bigData[i].time,
              imgurl: bigData[i].imgurl,
              note: bigData[i].note,
              isChange: false,
            },
          });

          if (
            bigData[i].Student.id !== bigData[nextI].Student.id ||
            i == bigData.length - 1
          ) {
            Object.assign(obj, {
              studentname: bigData[i].Student.name,
              studentid: bigData[i].Student.id,
              orgunit: bigData[i].Student.orgunit,
              id: count,
            });
            count++;
            arr.push(obj);
            obj = {};
          }
        }

        res.status(200).json({
          status: "success",
          data: arr,
        });
      } else {
        res.status(200).json({
          status: "success",
          data: [],
        });
      }
    } else {
      res.status(200).json({
        status: "success",
        data: [],
      });
    }
  });

  containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
      if (list[i] === obj) {
        return true;
      }
    }

    return false;
  }

  createStudentAttend = catchAsync(async (req, res, next) => {
    try {
      const { date, bool, studentid } = req.body;

      const date1 = new Date(date[0]);
      const date2 = new Date(date[1]);
      const difference = date2.getTime() - date1.getTime();
      const days = Math.ceil(difference / (1000 * 3600 * 24));
      const holiday = await this.models.Date.findAll({
        order: [["date", "ASC"]],
      });
      const holidayArr = holiday.map((el) => el.date.toString());
      let body = {};

      for (let i = 0; i <= days; i++) {
        if (bool == 2) {
          await this.models.StudentAttendance.destroy({
            where: { date: date1, studentid },
          });
        }
        if (
          this.containsObject(moment(date1).format("YYYY-MM-DD"), holidayArr)
        ) {
          console.log("first");
          body = {
            status: "NL",
            studentid: req.body.studentid,
          };
        } else if (
          date1.getDay() == 0 &&
          !this.containsObject(moment(date1).format("YYYY-MM-DD"), holidayArr)
        ) {
          body = {
            status: "CN",
            studentid: req.body.studentid,
          };
        } else if (
          date1.getDay() == 6 &&
          !this.containsObject(moment(date1).format("YYYY-MM-DD"), holidayArr)
        ) {
          body = {
            status: "T7",
            studentid: req.body.studentid,
          };
        } else {
          body = { ...req.body };
        }
        await this.models.StudentAttendance.create({
          ...body,
          date: date1,
        });
        body = {};
        date1.setDate(date1.getDate() + 1);
      }
      res.status(201).json({
        status: "success",
      });
    } catch (error) {
      next();
    }
  });

  updateStudentAttend = catchAsync(async (req, res, next) => {
    const doc = await this.models.StudentAttendance.update(req.body, {
      where: { id: req.params.id },
    });
    res.status(200).json({
      status: "success",
      data: doc,
    });
  });

  deleteStudentAttend = catchAsync(async (req, res, next) => {
    const doc = await this.models.StudentAttendance.destroy({
      where: { id: req.params.id },
    });
    if (!doc) {
      return next(new AppError(`No date found with that ID`, 404));
    }
    res.status(200).json({
      status: "success",
      data: null,
    });
  });

  monthDiff = (date1, date2) => {
    const d1 = new Date(date1);
    const d2 = new Date(date2);
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth() + 1;
    return months <= 0 ? 0 : months;
  };

  exportExcelFile = catchAsync(async (req, res, next) => {
    const option = await this.client.query(
      `select * from t_option where optiongroupid = 1`
    );
    const options = option[0];

    const workbook = new excelJS.Workbook();
    const paths = "./files";
    const { fromDay, toDay } = req.query;
    const numMonth = this.monthDiff(fromDay, toDay);
    const inputYear = new Date(fromDay).getFullYear();
    const monthArr = [];
    for (let i = 0; i < numMonth; i++) {
      monthArr.push({
        from: new Date(fromDay),
        to: new Date(toDay),
        fromDay,
        toDay,
      });
    }

    for (let el of monthArr) {
      const from_day = el.from;
      let doc;
      const bigData = [];

      for (let org of orgunit) {
        doc = await this.models.StudentAttendance.findAll({
          attributes: { exclude: ["studentid", "StudentId"] },
          where: {
            date: {
              [Op.gte]: el.fromDay,
              [Op.lte]: el.toDay,
            },
          },
          include: [
            {
              model: this.models.Student,
              where: {
                orgunit: org,
              },
              attributes: ["id", "code", "name", "orgunit"],
            },
          ],
          order: [
            ["Student", "name", "ASC"],
            ["date", "ASC"],
          ],
        });
        bigData.push(doc);
      }

      const arrDays = [];
      const date1 = el.from;
      const date2 = el.to;
      const difference = date2.getTime() - date1.getTime();
      const days = Math.ceil(difference / (1000 * 3600 * 24));
      for (let i = 0; i <= days; i++) {
        arrDays.push(date1.toString());
        date1.setDate(date1.getDate() + 1);
      }

      const numNL = bigData[0]
        .slice(0, days + 1)
        .filter(
          (el) => el.status === "T7" || el.status === "CN" || el.status === "NL"
        ).length;

      studentAttend({
        from_day,
        days,
        arrDays,
        bigData,
        workbook,
        options,
        numNL,
      });
    }

    try {
      await workbook.xlsx.writeFile(`${paths}/userAttendance.xlsx`).then(() => {
        const file = `${paths}/userAttendance.xlsx`;
        res.download(file);
      });
    } catch (err) {
      console.log(err);
      res.send(err);
    }
  });

  getAllStudentAtttend = catchAsync(async (req, res, next) => {
    const { fromDay, toDay } = req.params;
    if (fromDay !== "null" && toDay !== "null") {
      const date1 = new Date(fromDay);
      const date2 = new Date(toDay);
      const difference = date2.getTime() - date1.getTime();
      const days = Math.ceil(difference / (1000 * 3600 * 24));
      const doc1 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${fromDay}', '${toDay}')`
      );

      const bigData = [...doc1[0]];

      Array.prototype.chunk = function (size) {
        let result = [];

        while (this.length) {
          result.push(this.splice(0, size));
        }

        return result;
      };

      const newArr = bigData.chunk(days + 1);
      let arr = [];

      newArr.forEach((el, bigIndex) => {
        let obj = {
          studentname: el[0].studentname,
          studentid: el[0].student_id,
          orgunit: el[0].orgunit,
          id: bigIndex + 1,
        };
        el.forEach((item, index) => {
          Object.assign(obj, {
            [new Date(item.eventdate).getDate()]: {
              attendance_id: item.attendance_id,
              val: item.attendance,
              eventdate: item.eventdate,
              eventtime: item.eventtime,
              imgurl: item.imgurl,
              note: item.attendance_note,
              isChange: false,
              isupdated: item.isupdated,
            },
          });
        });
        arr.push(obj);
        obj = {};
      });

      res.status(200).json({
        status: "success",
        data: arr,
      });
    } else {
      res.status(200).json({
        status: "success",
        data: [],
      });
    }
  });

  getColDate = catchAsync(async (req, res, next) => {
    const { fromDay, toDay } = req.params;
    if (fromDay !== "null" && toDay !== "null") {
      const options = await this.client.query(
        `select * from t_option where optiongroupid = 1`
      );
      const col = [];
      for (
        let i = new Date(fromDay).getDate();
        i <= new Date(toDay).getDate();
        i++
      ) {
        col.push({ d: i });
      }
      res.status(200).json({
        status: "success",
        data: col,
        options: options[0],
      });
    } else {
      res.status(200).json({
        status: "success",
        data: [],
      });
    }
  });

  saveStudentAttend = catchAsync(async (req, res, next) => {
    const { data, data2 } = req.body;
    await this.models.StudentAttendance.bulkCreate(data);
    data2.forEach(async (el) => {
      await this.models.StudentAttendance.update(
        { status: el.status },
        {
          where: { id: el.id },
        }
      );
    });
    res.status(201).json({
      status: "success",
    });
  });
}

module.exports = StudentAttendController;
