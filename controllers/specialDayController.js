// const pool = require("../db");
// const axios = require("axios");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const { getUnique } = require("../utils/functionHelper");
const { Op } = require("sequelize");

const Models = require("../models/sequelize");
const axios = require("axios");

class SpecialDayController {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  getSpecialDays = catchAsync(async (req, res, next) => {
    const { fromDay, toDay } = req.query;
    let where = {};
    if (fromDay && toDay)
      Object.assign(where, {
        date: {
          [Op.gte]: fromDay,
          [Op.lte]: toDay,
        },
      });
    const doc = await this.models.Date.findAll({
      where,
      order: [["date", "ASC"]],
    });
    res.status(200).json({
      status: "success",
      data: doc,
    });
  });

  createSpecialDay = catchAsync(async (req, res, next) => {
    const { date, name, note } = req.body;
    const date1 = new Date(date[0]);
    const date2 = new Date(date[1]);
    const difference = date2.getTime() - date1.getTime();
    const days = Math.ceil(difference / (1000 * 3600 * 24));
    for (let i = 0; i <= days; i++) {
      await this.models.Date.create({ date: date1, name, note });
      date1.setDate(date1.getDate() + 1);
    }
    res.status(201).json({
      status: "success",
      // data: doc,
    });
  });

  updateSpecialDay = catchAsync(async (req, res, next) => {
    const doc = await this.models.Date.update(req.body, {
      where: { id: req.params.id },
    });
    res.status(200).json({
      status: "success",
      data: doc,
    });
  });

  deleteSpecialDay = catchAsync(async (req, res, next) => {
    const doc = await this.models.Date.destroy({
      where: { id: req.params.id },
    });
    if (!doc) {
      return next(new AppError(`No date found with that ID`, 404));
    }
    res.status(200).json({
      status: "success",
      data: null,
    });
  });

  addGoogleHoliday = catchAsync(async (req, res, next) => {
    await axios
      .get(
        "https://www.googleapis.com/calendar/v3/calendars/en.vietnamese%23holiday%40group.v.calendar.google.com/events?key=AIzaSyCNIzomy3VMUDZqqxjR1eA2qKMbhAHqodM"
      )
      .then(async (data) => {
        const holiday = [];
        const arr = [];
        for (let i of data.data.items) {
          const year = parseInt(i.start.date.split("-")[0]);
          if (year == req.params.year) {
            holiday.push({
              summary: i.summary,
              description: i.description,
              start: i.start.date,
              end: i.end.date,
            });
          }
        }

        for (let day of holiday) {
          const date1 = new Date(day.start);
          const date2 = new Date(day.end);
          const difference = date2.getTime() - date1.getTime();
          const days = Math.ceil(difference / (1000 * 3600 * 24));
          for (let i = 0; i <= days - 1; i++) {
            arr.push({
              date: date1.toString(),
              name: day.summary,
              note: day.description,
            });
          }
        }
        const newArr = getUnique(arr, "date");
        for (let i of newArr) {
          await this.models.Date.create({
            date: new Date(i.date),
            name: i.name,
            note: i.note,
          });
        }

        res.status(201).json({
          status: "success",
        });
      });
  });
}

module.exports = SpecialDayController;
