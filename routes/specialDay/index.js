const express = require("express");
const router = express.Router();

const SpecialDayController = require("../../controllers/specialDayController");

module.exports = (config) => {
  const specialDayController = new SpecialDayController(config.postgres.client);

  router
    .route("/")
    .get(specialDayController.getSpecialDays)
    .post(specialDayController.createSpecialDay);

  router
    .route("/:id")
    .patch(specialDayController.updateSpecialDay)
    .delete(specialDayController.deleteSpecialDay);

  router.post("/holiday/:year", specialDayController.addGoogleHoliday);

  return router;
};
