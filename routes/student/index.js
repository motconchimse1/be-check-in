const express = require("express");
const router = express.Router();

const StudentController = require("../../controllers/studentController");

module.exports = (config) => {
  const studentController = new StudentController(config.postgres.client);

  router.route("/").get(studentController.getAllStudent);

  return router;
};
