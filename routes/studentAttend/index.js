const express = require("express");
const router = express.Router();

const StudentAttendController = require("../../controllers/studentAttendController");
const HandleFactory = require("../../controllers/handleFactory");

module.exports = (config) => {
  const studentAttendController = new StudentAttendController(
    config.postgres.client
  );

  const handleFactory = new HandleFactory(config.postgres.client);

  router
    .route("/")
    .get(studentAttendController.getStudentAttends)
    .post(studentAttendController.createStudentAttend);


  router
    .route("/:id")
    .patch(studentAttendController.updateStudentAttend)
    .delete(studentAttendController.deleteStudentAttend);

  router.post("/save", studentAttendController.saveStudentAttend);
  router.post("/check", handleFactory.checkStudentAttend);

  router.get("/downloadExcel", studentAttendController.exportExcelFile);
  router.get(
    "/allAttendance/:fromDay/:toDay",
    studentAttendController.getAllStudentAtttend
  );
  router.get(
    "/allAttendance2/:fromDay/:toDay",
    studentAttendController.getStudentAttends2
  );
  router.get("/dates/:fromDay/:toDay", studentAttendController.getColDate);
  return router;
};
