const { DataTypes } = require("sequelize");
const catchAsync = require("../../utils/catchAsync");

const ModelCreate = (sequelize) => {
  const Date = sequelize.define(
    "Date",
    {
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        unique: true,
      },
      name: {
        type: DataTypes.STRING, 
        allowNull: false,
      },
      note: {
        type: DataTypes.STRING,
      },
    },
    {
      tableName: "t_specialday",
      timestamps: false,
    }
  );

  const Student = sequelize.define(
    "Student",
    {
      // id: {
      //   type: DataTypes.INTEGER,
      //   autoIncrement: true,
      //   primaryKey: true,
      // },
      code: {
        type: DataTypes.STRING(50),
        // unique: true,
      },
      name: {
        type: DataTypes.STRING(50),
      },
      gender: {
        type: DataTypes.BOOLEAN,
      },
      orgunit: {
        type: DataTypes.STRING(512),
      },
    },
    { tableName: "t_student", timestamps: false }
  );

  const StudentAttendance = sequelize.define(
    "StudentAttendance",
    {
      status: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      note: {
        type: DataTypes.STRING,
      },
      createdby: {
        type: DataTypes.STRING,
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
      time: {
        type: DataTypes.TIME,
      },
      imgurl: {
        type: DataTypes.TEXT,
      },
      trial994: {
        type: DataTypes.STRING(1),
      },
      // studentid: {
      //   type: DataTypes.INTEGER,
      // },
    },
    {
      tableName: "t_student_attendance",
      timestamps: true,
      createdAt: "createdat",
      updatedAt: "updatedat",
    }
  );

  Student.hasMany(StudentAttendance);

  StudentAttendance.belongsTo(Student, { foreignKey: "studentid" });

  // sequelize.sync({ alter: true });
};

module.exports = ModelCreate;
