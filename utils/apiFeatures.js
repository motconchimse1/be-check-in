class APIFeatures {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  filter() {
    // 1A) Filtering
    const queryObj = { ...this.queryString };
    const exludedFields = ["page", "per_page", "sort", "fields", "name"];
    exludedFields.forEach((el) => delete queryObj[el]);

    // 1B) Advanced filtering
    let queryStr = JSON.stringify(queryObj);

    queryObj = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    this.query = this.query.find(JSON.parse(queryStr));

    return this;
  }

  sort() {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(",").json(" ");
      this.query = this.query.sort(sortBy);
    } else {
      this.query = this.query.sort("-createdat");
    }
    return this;
  }

  limitFields() {
    if (this.queryString.fields) {
      const fields = this.queryString.fields.split(",").join(" ");
      this.query = this.query.select(fields);
    } else {
      this.query = this.query.select("trial994");
    }
    return this;
  }

  pagination() {
    const page = this.queryString.page * 1 || 1;
    const per_page = this.queryString.per_page * 1 || 20;
    const skip = (page - 1) * per_page;

    this.query = this.query.skip(skip).limit(per_page);
  }
}
