const Logger = require("bunyan");
const bunyan = require("bunyan");
// Load package.json
const pjs = require("../package.json");

// Get some meta info from the package.json
const { name, version } = pjs;

// Set up a logger
const getLogger = (serviceName, serviceVersion, level) =>
  bunyan.createLogger({ name: `${serviceName}:${serviceVersion}`, level });

// Configuration options for different environments
module.exports = {
  development: {
    name,
    version,
    serviceTimeout: 30,
    postgres: {
      options: {
        // host: "42.117.5.115",
        // port: 5432,
        // database: "DienToanFace",
        host: "192.168.1.101",
        port: 5438,
        database: "BKDOR",
        dialect: "postgres",
        username: "vietnguyen",
        password: "123456",
        logging: (msg) => getLogger(name, version, "debug").info(msg),
      },
      client: null,
    },
    log: () => getLogger(name, version, "debug"),
  },
  production: {
    name,
    version,
    serviceTimeout: 30,
    log: () => getLogger(name, version, "info"),
  },
  test: {
    name,
    version,
    serviceTimeout: 30,
    log: () => getLogger(name, version, "fatal"),
  },
};
